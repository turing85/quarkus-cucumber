package de.turing85.quarkus.cucumber.integration.actors;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

import de.turing85.quarkus.cucumber.integration.config.SutUriProvider;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import java.net.URI;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.Response.Status;

@ApplicationScoped
public class HttpActor {

  private final SutUriProvider sutUriProvider;
  private ValidatableResponse lastResponse;

  public HttpActor(SutUriProvider sutUriProvider) {
    this.sutUriProvider = sutUriProvider;
  }

  public void getPlainText(URI endpoint) {
    lastResponse = given()
        .when()
            .accept(ContentType.TEXT)
            .get(sutUriProvider.sutUri().resolve(endpoint))
        .then();
  }

  public void expectLastResponseIs(String expected) {
    lastResponse
        .statusCode(Status.OK.getStatusCode())
        .body(is(expected));
  }
}
