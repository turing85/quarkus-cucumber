package de.turing85.quarkus.cucumber.integration.steps;

import de.turing85.quarkus.cucumber.integration.actors.HttpActor;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.net.URI;

public class HttpSteps {

  private final HttpActor actor;

  public HttpSteps(HttpActor actor) {
    this.actor = actor;
  }

  @When("I access endpoint {uri}")
  public void whenIAccessEndpoint(URI endpoint) {
    actor.getPlainText(endpoint);
  }

  @Then("I receive the text {string}")
  public void thenIExpectToReceive(String expectedText) {
    actor.expectLastResponseIs(expectedText);
  }
}
