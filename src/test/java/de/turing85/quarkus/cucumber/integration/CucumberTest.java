package de.turing85.quarkus.cucumber.integration;

import io.quarkiverse.cucumber.CucumberQuarkusTest;

public class CucumberTest extends CucumberQuarkusTest {

  public static void main(String[] args) {
    runMain(CucumberTest.class, args);
  }
}