package de.turing85.quarkus.cucumber.integration.config;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@ApplicationScoped
public class SutUriProvider {

  private static final String LOCALHOST = "http://localhost";

  private final URI sutUri;

  public SutUriProvider(
      @ConfigProperty(name = "quarkus.http.test-port") int testPort) {
    sutUri = URI.create(Optional.ofNullable(System.getProperty("quarkus.http.test-host"))
        .map(SutUriProvider::prefixHostWithHttpIfNecessary)
        .orElse("%s:%d".formatted(LOCALHOST, testPort)));
  }

  private static String prefixHostWithHttpIfNecessary(String host) {
    if (Objects.isNull(host)) {
      return null;
    }
    return host.startsWith("http") ? host : "http://%s".formatted(host);
  }

  public final URI sutUri() {
    return sutUri;
  }
}
