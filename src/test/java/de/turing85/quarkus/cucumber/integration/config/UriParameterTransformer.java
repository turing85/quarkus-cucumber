package de.turing85.quarkus.cucumber.integration.config;

import io.cucumber.java.ParameterType;
import java.net.URI;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class UriParameterTransformer {

  @ParameterType(name = "uri", value = ".*")
  public URI uri(String uriAsString) {
    return URI.create(uriAsString.startsWith("/") ? uriAsString : "/%s".formatted(uriAsString));
  }
}
