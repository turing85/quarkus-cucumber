# Quarkus Cucumber showcase

This project defines some Quarkus Cucumber REST tests. The tests are implemented with 
[rest-assured][restAssuredHome]. The speciality of this showcase is that it lets us runt he cucumber
tests against an already deployed version of the service.

## Technical implementation
We use the [`quarkus-cucumber` quarkiverse extension][quarkusCucumberHome] to implement our cucumber
tests. The core philosophy of this extension is that our test class (in this repository, 
[`CucumberTest.java`][cucumberTestJava]) `extends CucumberQuarkusTest`. This, in turn, marks
the test as `@QuarkusTest` (see [the official guide][quarkusTestingGuide] for details), and we can
use dependency injection. The tests, however, are alway run against a quarkus instance boostrapped 
for the tests, i.e. we cannot run the tests against an already deployed version of our application.
Further we cannot use the 
["Executing against a running application"-feature from `@QuarkusIntegrationTest`][quarkusTestingGuide_16_2]
since marking the test as `@QuarkusIntegrationTest` disables Dependency Injection, and we need 
dependency injection in order to transfer state from one step to another.

To relief this shortcoming, we:

- create a class [`SutUriProvider.java`][sutUriProviderJava] that
  - reads system property `quarkus.http.test-host` and, if set
  - then send all requests against the URI `"http://<test-host>"`
    (the String `"http://"` can be the start of the `test-host`. If `test-host` does not start with 
    the string `"http"`, it will be prefixed with `"http://`)
- Otherwise, all requests are sent against `http://localhost:<quarkus.http.test-port>`.

## Building and testing
After checkout, we can build and test the project by calling

Linux/Unix:
```shell
./mvnw test
```

Windows:
```shell
mvnw.cmd test
```

We will see the following output near the end of the tests:

```
...
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
[INFO] Running de.turing85.quarkus.cucumber.integration.CucumberTest
Nov. 28, 2021 2:35:40 PM org.jboss.threads.Version <clinit>
INFO: JBoss Threads version 3.4.2.Final
Nov. 28, 2021 2:35:42 PM org.jboss.threads.Version <clinit>
INFO: JBoss Threads version 3.4.2.Final
Nov. 28, 2021 2:35:43 PM io.quarkus.bootstrap.runner.Timing printStartupTime
INFO: Quarkus 2.5.0.Final on JVM started in 4.152s. Listening on: http://localhost:8081
Nov. 28, 2021 2:35:43 PM io.quarkus.bootstrap.runner.Timing printStartupTime
INFO: Profile test activated.
Nov. 28, 2021 2:35:43 PM io.quarkus.bootstrap.runner.Timing printStartupTime
INFO: Installed features: [cdi, cucumber, resteasy, smallrye-context-propagation, vertx]

Scenario: I can get the expected response from hello endpoint # de/turing85/quarkus/cucumber/foo.feature:3
  When I access endpoint hello                                # de.turing85.quarkus.cucumber.integration.steps.HttpSteps.whenIAccessEndpoint(java.net.URI)
  Then I receive the text "Hello RESTEasy"                    # de.turing85.quarkus.cucumber.integration.steps.HttpSteps.thenIExpectToReceive(java.lang.String)


1 Scenarios (1 passed)
2 Steps (2 passed)
0m2.070s
...
```

## Executing against a deployed service
In order to test the execution of the deployed service, we will 

- package the quarkus application:  
  Linux/Unix:
  ```shell
  ./mvnw -DskipTests package
  ```
  Widows:
  ```shell
  mvnw.cmd -DskipTests package
  ```
- copy the application into another directory:  
  Linux/Unix:
  ```shell
  mkdir -p /some/temporary/directory
  cp -R target/quarkus-app/* /some/temporary/directory/
  ```
  Windows:
  ```shell
  mkdir C:\some\temporary\directory
  xcopy /E /Y .\target\quarkus-app\ C:\some\temporary\directory
  ```
- change in the temoprary directory:  
  Linux/Unix:
  ```shell
  cd /some/temporary/directory/
  ```
  Windows:
  ```shell
  cd C:\some\temporary\directory
  ```
- launch the application on port `8090`:
  ```shell
  java -Dquarkus.http.port=8090 -jar quarkus-run.jar
  ```
- edit the [`GreetingResource.java`][greetingResourceJava] to change the text it returns:
  ```diff
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
  -   return "Hello RESTEays"; 
  +   return "Hello";
    }
  ```

If we open a new terminal and re-execute the tests as we did before (Linux/Unix: `./mvnw test`, 
Windows: `mvnw.cmd test`), we will see a failing test since we have just changed the text returned by 
the endpoint, but we did not modify the test:

```
...
Caused by: java.lang.AssertionError:
1 expectation failed.
Response body doesn't match expectation.
Expected: is "Hello RESTEasy"
  Actual: Hello


Nov. 28, 2021 3:13:02 PM io.quarkus.bootstrap.runner.Timing printStopTime
INFO: Quarkus stopped in 0.089s
[INFO]
[INFO] Results:
[INFO]
[ERROR] Failures:
[ERROR] de.turing85.quarkus.cucumber.integration.CucumberTest.getTests
[INFO]   Run 1: PASS
[INFO]   Run 2: PASS
[ERROR]   Run 3: CucumberTest>CucumberQuarkusTest.lambda$getTests$8:154 failed in classpath:de/turing85/quarkus/cucumber/foo.feature at line 5
[INFO]   Run 4: PASS
[INFO]
[INFO]
[ERROR] Tests run: 1, Failures: 1, Errors: 0, Skipped: 0
[INFO]
[INFO] ------------------------------------------------------------------------
[INFO] BUILD FAILURE
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  23.071 s
[INFO] Finished at: 2021-11-28T15:13:03+01:00
[INFO] ------------------------------------------------------------------------
...
```

But the application we have started on port `8090` still has the cold version running that is 
compatible with the tests. To verify that we can run the tests against an already running deployment
we will now execute the tests against `localhost:8090`:  
Linux/Unix:
```shell
./mvnw -Dquarkus.http.test-host=localhost:8090 test
```
Windows:
```shell
mvnw -Dquarkus.http.test-host=localhost:8090 test
```

And we should again see our tests passing:
```
...
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
[INFO] Running de.turing85.quarkus.cucumber.integration.CucumberTest
Nov. 28, 2021 3:17:32 PM org.jboss.threads.Version <clinit>
INFO: JBoss Threads version 3.4.2.Final
Nov. 28, 2021 3:17:35 PM io.quarkus.runtime.configuration.ConfigDiagnostic unknown
WARN: Unrecognized configuration key "quarkus.http.test-host" was provided; it will be ignored; verify that the dependency extension for this configuration is set or that you did not make a typo
Nov. 28, 2021 3:17:35 PM org.jboss.threads.Version <clinit>
INFO: JBoss Threads version 3.4.2.Final
Nov. 28, 2021 3:17:35 PM io.quarkus.bootstrap.runner.Timing printStartupTime
INFO: Quarkus 2.5.0.Final on JVM started in 3.952s. Listening on: http://localhost:8081
Nov. 28, 2021 3:17:35 PM io.quarkus.bootstrap.runner.Timing printStartupTime
INFO: Profile test activated.
Nov. 28, 2021 3:17:35 PM io.quarkus.bootstrap.runner.Timing printStartupTime
INFO: Installed features: [cdi, cucumber, resteasy, smallrye-context-propagation, vertx]

Scenario: I can get the expected response from hello endpoint # de/turing85/quarkus/cucumber/foo.feature:3
  When I access endpoint hello                                # de.turing85.quarkus.cucumber.integration.steps.HttpSteps.whenIAccessEndpoint(java.net.URI)
  Then I receive the text "Hello RESTEasy"                    # de.turing85.quarkus.cucumber.integration.steps.HttpSteps.thenIExpectToReceive(java.lang.String)


1 Scenarios (1 passed)
2 Steps (2 passed)
0m1.674s
...
```

## Todos

- As of now, cucumber tests do not recognize the `quarkus.http.port` set in the dev profile
- Although we might not need it, the tests start a quarkus test application nevertheless.
- It would be nice to have an annotation similar to `@ScenarioScoped` provided by `cucumber-spring`

[restAssuredHome]: https://rest-assured.io/
[quarkusCucumberHome]: https://github.com/quarkiverse/quarkus-cucumber
[cucumberTestJava]: ./src/test/java/de/turing85/quarkus/cucumber/integration/CucumberTest.java
[quarkusTestingGuide]: https://quarkus.io/guides/getting-started-testing#recap-of-http-based-testing-in-jvm-mode4
[quarkusTestingGuide_16_2]: https://quarkus.io/guides/getting-started-testing#executing-against-a-running-application
[sutUriProviderJava]: ./src/test/java/de/turing85/quarkus/cucumber/integration/config/SutUriProvider.java
[greetingResourceJava]: ./src/main/java/de/turing85/quarkus/cucumber/GreetingResource.java